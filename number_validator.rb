module NumberValidator
  DESCRIPTION_TEMPLATE = "Fly Buys #"
  def valid?
    return false if card_type == :unknown
    valid_card_number?
  end

  def card_type
    return :black if is_black?

    # For all other card types, the number *must* be 16 digits and anything
    # else would be unknown. So, we can
    return :unknown if number_length != 16

    return :red if is_red?
    return :green if is_green?
    return :blue if is_blue?
    return :unknown
  end

  def is_black?
    number_length.between?(16,17)
    number.to_s.start_with?("60141")
  end

  def is_red?
    number.to_s.start_with?("6014352")
  end

  def is_green?
    # Green card numbers fall into a range, rather than a specific prefix check
    green_prefix = digits[0..9].join.to_i
    green_prefix.between?(6014355526, 6014355529)
  end

  def is_blue?
    number.to_s.start_with?("6014")
  end

  def valid_card_number?
    double_digits = number.digits.map.with_index do |digit, index|
      index.odd? ? digit * 2 : digit
    end
    (double_digits.join.to_i.digits.sum % 10) == 0
  end

  def description
    type = card_type
    return "#{type.to_s.capitalize}: #{number} (invalid)" if type == :unknown

    "Fly Buys #{type.to_s.capitalize}: #{number} (#{valid? ? "valid" : "invalid"})"
  end
end
