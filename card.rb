require_relative "number_validator"

class Card
  include NumberValidator

  attr_reader :number

  def initialize(number)
    @number = format_number(number)
  end

  def format_number(number)
    return number if number.is_a? Integer
    return number.gsub(/\s+/, "").to_i if number.is_a? String
  end

  def digits
    number.digits.reverse
  end

  def number_length
    digits.length
  end
end
