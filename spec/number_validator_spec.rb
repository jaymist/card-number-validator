require "spec_helper"
require_relative "../card"

describe "card" do
  [
    [60141016700078611, :black, true, "Fly Buys Black: 60141016700078611 (valid)"],
    [6014152705006141, :black, false, "Fly Buys Black: 6014152705006141 (invalid)"],
    [6014111100033006, :black, true, "Fly Buys Black: 6014111100033006 (valid)"],
    [6014709045001234, :blue, true, "Fly Buys Blue: 6014709045001234 (valid)"],
    [6014352700000140, :red, true, "Fly Buys Red: 6014352700000140 (valid)"],
    [6014355526000020, :green, true, "Fly Buys Green: 6014355526000020 (valid)"],
    ["6014 3555 2900 0028", :green, false, "Fly Buys Green: 6014355526000028 (invalid)"],
    [6013111111111111, :unknown, false, "Unknown: 601311111111111 (invalid)"],
  ].each do |number, type, valid, description|
    it "checks the type of '#{number}' is #{type}" do
      card = Card.new(number)
      expect(card.card_type).to equal(type)
    end

    it "checks if #{number} is #{valid ? "valid" : "invalid"}" do
      card = Card.new(number)
      expect(card.valid?).to be valid
    end

    it "should generate a full description for #{number}" do
      card = Card.new(number)
      description = card.description
      expect(card.description).to eq(description)
    end
  end
end