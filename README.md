# Card Number Validator

This is a simple module to validate that the provided card number is part of the Flybuys scheme.  The module's behaviour is tested with [rspec](https://rspec.info/) and you can run the tests with the following command:

```
bundle exec rspec
```